       $(document).ready(function () {
            var navListItems = $('div.setup-panel div a'),
                    allWells = $('.setup-content'),
                    allNextBtn = $('.nextBtn');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                        $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-primary').addClass('btn-default');
                    $item.addClass('btn-primary');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allNextBtn.click(function () {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url'],textarea[textarea]"),
                    isValid = true;

                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

            $('div.setup-panel div a.btn-primary').trigger('click');
        });

        var formdata = {};
        var Requestdata = {};
        function onfinish()
            //$('#loanform').submit(function()
        {
            var values = {};
            var $inputs = $('#loanform :input');
            $inputs.each(function () {
                values[this.id] = $(this).val();
            });
            formdata = values;
            SubmitRequest();

        }
        function SubmitRequest() {
            Requestdata =
            {
                "BusinessTaxID": formdata.BusinessTaxID,
                "Email": formdata.Email,
                "LegalBusinessName": formdata.LegalBusinessName,
                "RequestedAmount": "7000",
                "PurposeOfLoan": "1",
                "ExpectedTimeOfApproval": "1",
                "AddressLine1": formdata.AddressLine1,
                "City": formdata.City,
                "State": formdata.State,
                "PostalCode": formdata.PostalCode,
                "AddressType": "1",
                "SICCode": "1234",
                "BusinessStartDate": formdata.BusinessStartDate,
                "BusinessType": formdata.BusinessType,
                "FirstName": formdata.FirstName,
                "LastName": formdata.LastName,
                "SSN": formdata.SSN,
                "DateOfBirth": formdata.DateOfBirth,
                "Ownership": formdata.Ownership,
                "HomeAddressLine1": formdata.HomeAddressLine1,
                "HomeCity": formdata.HomeCity,
                "HomeState": formdata.HomeState,
                "HomePostalCode": formdata.PostalCode,
                "Mobile": "9876543210",
                "AnnualRevenue": formdata.AnnualRevenue,
                "AverageBankBalances": formdata.AverageBankBalances,
                "HaveExistingLoan": "false",
                "DateNeeded": "12/12/2017"              
		
            }

            var token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDEtMTFUMDU6MjI6MTEuNzg5MDQ3WiIsImV4cCI6IjIwMTctMDEtMTFUMDU6MzI6MTEuNzg3NTAxWiIsInN1YiI6ImNhdnAiLCJ0ZW5hbnQiOiJjYXBpdGFsLWFsbGlhbmNlIiwic2NvcGUiOlsiYXBpL3Zwb2ZzYWxlcyIsImFwaS9sb2FuY29vcmRpbmF0b3JtYW5hZ2VyIiwiYXBpL2xvYW5jb3JkaW5hdG9yIiwiYXBpL3NycmVsYXRpb25zaGlwbWFuYWdlciIsImFwaS9yZWxhdGlvbnNoaXBtYW5hZ2VyIl0sIklzVmFsaWQiOnRydWV9.ER1BnvTkFxlC0a1h4WiTStUCPEhNqb1p-DO2mqbYV5s";

            try {
                $.ajax({
                    url: 'http://192.168.1.67:6012/application/submit/business',

                    method: 'POST',
                    data: JSON.stringify(Requestdata),
                    cache: false,
                    contentType: 'application/json',
                    dataType: 'json',
                    beforeSend: function (xhr) {

                        xhr.setRequestHeader('Authorization', token);
                        xhr.setRequestHeader('Content-Type', 'application/json');
                        xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
                        xhr.setRequestHeader('Access-Control-Allow-Credentials', true);

                        xhr.setRequestHeader('Access-Control-Allow-Headers', 'Authorization, Lang');
                        xhr.setRequestHeader('Access-Control-Allow-Credentials', 'POST,GET,PUT,DELETE');

                    },

                    success: function (data) {
                        console.log("Succesfully Request submited");
                        console.log(data);
						localstorage.setItem('myMainKey', data);
                        window.location.href = "main.html";
                    },
                    error: function (jqXHR, xhr, status, errorThrown) {
                        if (xhr.status == 401) {

                        }
                        else {
                            var r = jQuery.parseJSON(xhr.responseText);

                        }
                    }
                });
            }
            catch (e) {

            }
        }